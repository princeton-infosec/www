#!/usr/bin/env bash

rm -rf resources public
hugo --i18n-warnings server --disableFastRender
