---
# Display name
name: Joseph Eichenhofer

# Username (this should match the folder name)
authors:
- jeichenhofer

# Is this the primary user of the site?
superuser: true

# Role/position
role: Assistant Instructor

email: josephoe@cs.princeton.edu

# List office hour information
office_hours:
- TW 1:30PM - 2:30PM
- 003 Computer Science
  
# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.  
user_groups:
- Instructors
--- 
