---
# Display name
name: Edward W. Felten

# Username (this should match the folder name)
authors:
- felten

# Is this the primary user of the site?
superuser: false

# Role/position
role: Professor

email: felten@cs.princeton.edu

# List office hour information
office_hours:
- TR 12:30PM - 1:30PM
- 302 Sherrerd Hall

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.  
user_groups:
- Instructors
--- 
