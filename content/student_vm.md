---
title: "Student Virtual Machine Environment"
summary:    "This tutorial will guide you through downloading and configuring a virtual machine for completing
            assignments in this course."

type: "page"

reading_time: false
share: false
profile: false
comments: false
---

This tutorial will guide you through downloading and configuring a virtual machine for completing assignments in this
course.

# Installing the Virtual Machine

 1. Download and install [VirtualBox](https://www.virtualbox.org/wiki/Downloads) for your platform.
 2. Download the {{< staticLink "vm_images/infosec_vm_distribution.ova" "infosec_vm_distribution.ova" >}}Student
    Virtual Machine{{< /staticLink >}} distribution file.
 3. Open VirtualBox.
 4. In the VirtualBox Manager window, navigate the menu to `File` -> `Import Appliance`.
 5. Click the folder/browse icon and find the `infosec_vm_distribution.ova` file you just downloaded.
 6. Click `Continue` or `Next`.
 7. Check the box next to `Reinitialize the MAC address of all network cards` and then click `Import`.
 8. Wait for the import process to complete.
 9. Click on the new virtual machine named "infosec_vm_distribution" to select it and click the `Start` button at the
    top of the screen.
10. You can now use the virtual machine as you would a normal Ubuntu computer. If you need to use an elevated
    command, the username and password are both `student`.
11. When you are done using the virtual machine, you can simply close the window. It will prompt you to choose one of
    the following options:
     * Save the machine state
     * Send the shutdown signal
     * Power off the machine
     
     In most cases, you can select the second option. The third option will force the machine to terminate, potentially
     destroying unsaved work.

# Destroying the Virtual Machine

If at any point, you need to start with a fresh virtual machine instance, you can follow these steps to delete the old
version. This process will completely destroy the virtual environment, including files saved onto the virtual disk. Do
not follow these steps if you have work saved in the virtual machine! After following these steps, you can repeat the
steps above to re-install the machine.

 1. Right click on the virtual machine named "infosec_vm_distribution" and click `Remove...` in the context menu.
 2. The "VirtualBox - Question" popup interface will ask if you want to delete files associated with the virtual
    machine. Click on the `Delete all files` button. **Important:** This will delete any work you have saved in the
    virtual machine.
