---
title: Resources
summary: "Brief description of resources for use during the course."

type: "page"

reading_time: false
share: false
profile: false
comments: false
---

### Piazza
[Piazza](https://www.piazza.com/) is the best way to ask about course materials. Please do not email questions to
individual staff members. This will be the location for all official course correspondence.

### Lecture Slides
Lecture Slides will be available after each lecture on the Piazza *resources* page.

### Office Hours
Office Hours are for discussions or questions that cannot be sufficiently answered on Piazza. For discussions about
lecture material, please visit the professor's office hours. For discussions about assignment material, please visit the
AI office hours.

### Java Development Kit
For assignments that require Java programming, you are expected to perform your own debugging and troubleshooting. The
documentation for [JDK8](https://docs.oracle.com/javase/8/docs/api/) will be an important resource for these
assignments.

Be sure you have installed an up-to-date JDK with JDK8 compatibility (you can get the JDK at [Java SE Development Kit 8
Downloads](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) on the Oracle website).
For assistance installing the JDK or using a Java development environment, see the [COS Lab
TAs](http://labta.cs.princeton.edu/) (not this course's staff).

### Student Virtual Machine

You will be asked to use a prepared virtual machine to complete some of the projects in this class. By using this
virtual machine, the consistent environment will more likely to produce solutions that work with our grading frameworks.
You can find installation instructions for this virtual machine [here]({{< ref "student_vm" >}}).

### Books about Security (for some different perspectives on security)

| Citation                                                                               | Comments                                                                                 |
|----------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------|
| Ross J. Anderson, *Security Engineering*. 2nd Edition. 2008                            | Covers security in general, with many non-computing examples.                            |
| Dieter Gollmann, *Computer Security*. 3rd Edition. 2011.                               | General security textbook. Good for what it covers, but doesn't cover everything.        |
| David Kahn, *The Codebreakers*. 1996.                                                  | History of cryptography. Not very technical.                                             |
| Steven Levy, *Crypto*. 2001.                                                           | History of cryptography since 1970. Non-technical.                                       |
| Niels Ferguson and Bruce Schneier, *Practical Cryptography*. 2003.                     | Focused coverage of applied cryptography, not the most rigorous.                         |
| John Viega and Gary McGraw, *Building Secure Software*. 2011.                          | How to write software that will have fewer security bugs.                                |
| Michal Zalewski, *The Tangled Web: A Guide to Securing Modern Web Applications*. 2011. | Web security. Strikes a good balance between conceptual exposition and technical detail. |
