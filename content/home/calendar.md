+++
# Blank widget.
widget = "blank"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 20  # Order that this section will appear.

title = "Course Calendar"
subtitle = ""
+++

The course staff will keep this Google calendar up to date with assignment deadlines, precept scheduling, and office
hour changes. You should check this calendar before coming to office hours to make sure you have the correct time and
location. It may also be convenient to import the calendar to your own calendar app to have the class schedule more
accessible.

{{< gdocs src="https://calendar.google.com/calendar/embed?src=rdo1ofde43rp5dtl93fa606gpk%40group.calendar.google.com&ctz=America%2FNew_York" >}}
