+++
# Blank widget.
widget = "blank"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 10  # Order that this section will appear.

title = "Course Description"
subtitle = ""
+++

In this course, we will introduce you to the theories and techniques behind secure computing and communication systems.
The lectures and hands-on assignments will cover a range of important security topics:

{{< columns col_count="2" >}}
 - Basic cryptography
 - Private and authenticated communication
 - Software security
 - Malware
 - Operating system protection
 - Network security
 - Web security
 - Physical security
 - Cryptocurrencies and blockchains
 - Privacy and anonymity
 - Usable security
 - Economics of security
 - Ethics of security
 - Legal and policy issues
{{< /columns >}}
