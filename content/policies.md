---
title: Course Policies
summary: "Summary of course policies."

type: "page"

reading_time: false
share: false
profile: false
comments: false
---

### Textbook
There is no required textbook for this course. If you are interested in books about computer security, there are
recommendations listed on the [Resources]({{< ref "resources" >}}) page.

### Lectures
You are expected to attend all lectures. They will be held **Tuesday and Thursday from 11:00AM to 12:20PM** in COS 104.

### Assignments
You will complete six assignments for this course. Three of them are programming assignments that must be completed
independently. The remaining three assignments will be collaborative assignments that must be completed in groups of two
or three.

Late assignments will lose 10% of its total value for each late day. Assignments will not be accepted after seven late
days. Late penalties will be waived only in the case of unforeseeable circumstances, as documented by your dean or
director of studies and the course staff's approval.

### Exam
A portion of your grade will depend on a final exam that tests your understanding of both lecture and assignment
materials. No makeup exam will be considered without a dean's recommendation.

### Collaboration Policy
Many forms of collaboration are allowed in this course. However, we expect you to submit your own work that demonstrates
both understanding and correct implementation. All collaboration on assignments must be acknowledged in your submission.
To avoid any unauthorized collaboration, please follow this summary of collaboration rules when working:

|                                     | your own group | course staff | previous students | other classmates | other |
|-------------------------------------|----------------|--------------|-------------------|------------------|-------|
| **discuss lecture content with...** | yes            | yes          | yes               | yes              | yes   |
| **show solutions to...**            | yes            | yes          | no                | no               | no    |
| **view solutions from...**          | yes            | no           | no                | no               | no    |
| **plagiarize code from...**         | no             | no           | no                | no               | no    |

**A note about StackExchange, Wikipedia, and online forums:**

Many of the topics in this course require self-teaching. This includes a lot of searching online for documentation and
information about algorithms, technologies, and techniques. Unfortunately, this is includes a wide gray area between
learning and plagiarism. Don't be afraid of searching for and using online resources that teach you how to do something
*that is not a direct requirement of an assignment*.

For example, you are free to copy and paste a method that converts integers to byte arrays in an assignment about
cryptography; just make sure to *cite your source* and thoroughly *understand the implementation*, as your graders will
not be lenient with copied code that is buggy. If you find yourself searching for implementations of a security-related
algorithm, then you are likely violating the collaboration policy. When you are unsure, ask one of the staff members.
