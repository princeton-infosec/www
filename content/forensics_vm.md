---
title: "Forensics Virtual Machine Setup"
summary:    "This tutorial will guide you through downloading and configuring the forensics virtual image in order to
            complete assignment six."

type: "page"

reading_time: false
share: false
profile: false
comments: false
---

This tutorial will guide you through downloading and configuring the forensics virtual image in order to complete
assignment six. You should already have installed the student virtual machine as described [Student Virtual Machine
Environment]({{< ref "student_vm" >}}).

# Installing the Virtual Machine

These steps will guide you through installing the virtual machine similarly to how you installed the student virtual
machine. This is to emulate "live analysis" of the suspect machine.

 1. Download the {{< staticLink "vm_images/infosec_forensics_release.ova" "infosec_forensics_release.ova" >}}Forensics
    Virtual Disk Release{{< /staticLink >}} distribution file. This is a large file and may take several minutes to
    download. It is strongly recommended that you do not delete this file after you are done importing the appliance, as
    you will likely need to repeat this process.
 2. Open VirtualBox.
 3. In the VirtualBox Manager window, navigate the menu to `File` -> `Import Appliance`.
 4. Click the folder/browse icon and find the `infosec_forensics_release.ova` file you just downloaded.
 5. Click `Continue` or `Next`.
 6. Check the box next to `Reinitialize the MAC address of all network cards` and then click `Import`.
 7. Wait for the import process to complete.
 8. If you want to conduct "live analysis," then you can start the virtual machine by clicking on the newly added
    "infosec_forensics_release" to select it and clicking the `Start` button at the top of the screen. This will boot
    the machine as if you had pressed the power button on a physical machine. It is recommended that you start with
    "dead analysis" before attempting live analysis.

# Connecting the Image to the Student VM

These steps will allow you to access the forensics virtual disk image from your other virtual machine. This
configuration is the equivalent to dead analysis of a hard drive that you attach to an already running computer. Be
careful with this configuration, because you will never want both virtual machines "running" at the same time.

 1. Shut down both of the virtual machines if either is running. Note that this is different from "saving the machine
    state." The VirtualBox manager should show that both are in the "Powered Off" state.
 2. Right click on the "infosec_vm_distribution" entry and click `Settings...` in the context menu.
 3. The settings window that opens should have a menu item on the left called `Storage`. Click that menu item to see the
    storage settings interface.
 3. Under `Storage Devices`, left click on the words `Controller: SATA`. Two icons with green plus symbols should
    appear. Next to the controller entry.
 4. Click on the rightmost of the two icons that appeared. It should look like a `hard drive with a green plus` in front
    of it. When you hover your cursor over it, the words "Adds hard disk." should appear as a tooltip.
 5. A "VirtualBox - Question" window will ask if you want to create a disk. You should select `Choose Existing Disk`.
 6. You should see a browsing interface open somewhere on your filesystem. You need to find the suspect virtual disk
    image, which is named `infosec_forensics_release/infosec_forensics_release-disk001.vmdk` in your default VirtualBox
    VM directory. Click `Open` once you have selected the correct `infosec_forensics_release-disk001.vmdk` file.
 7. In the storage settings window, click `Ok`.
 8. Start the "infosec_vm_distribution" virtual machine and open a terminal window. Run the command `sudo fdisk -l`.
    Somewhere in the output of that command, you should see a disk with `Disk identifier: 0x7fee7bf8`. For example:
    ```bash
    Disk /dev/sdb: 4 GiB, 4294967296 bytes, 8388608 sectors
    Units: sectors of 1 * 512 = 512 bytes
    Sector size (logical/physical): 512 bytes / 512 bytes
    I/O size (minimum/optimal): 512 bytes / 512 bytes
    Disklabel type: dos
    Disk identifier: 0x7fee7bf8

    Device     Boot   Start     End Sectors  Size Id Type
    /dev/sdb1          2048   77823   75776   37M 83 Linux
    /dev/sdb2         77824 7890943 7813120  3.7G 83 Linux
    /dev/sdb3  *    7890944 8386559  495616  242M 83 Linux
    ```
 9. Take note of the `/dev/` entry for the new disk partitions. In the example above, it is `/dev/sdb1` through
    `/dev/sdb2`, but yours may be different. Note that each different "Device" listed there is a partition of a hard
    disk. Using those entries, run the following commands to mount some of the partitions:
    ```bash
    sudo mkdir /mnt1 && sudo mount /dev/sdb1 /mnt1
    sudo mkdir /mnt2 && sudo mount /dev/sdb2 /mnt2
    sudo mkdir /mnt3 && sudo mount /dev/sdb3 /mnt3
    ```
10. You can now browse the new directory (e.g., `cd /mnt1`) as you would any other file system directory. You may find
    that some of these partitions do not mount correctly, and that is something you need to investigate as a part of the
    assignment.
11. When you want to run the suspect machine for "live analysis," be sure that you have shut down the
    "infosec_vm_distribution" virtual machine *before* trying to start the "infosec_forensics_release" virtual machine.

# Destroying the Virtual Machine

It is very likely that you will irreparably damage the state of the forensics virtual disk. This is why it is important
to keep the source `infosec_forensics_release.ova` on hand to restart with a fresh instance. Follow these steps to
completely destroy the virtual machine so you can re-configure it by following the above steps again.

 1. Shut down both virtual machines so you can safely manipulate the virtual disks.
 2. Navigate to the storage settings menu of the "infosec_vm_distribution" as you did to connect the suspect image.
 3. In the storage settings menu, under "Storage Devices," you should see `infosec_forensics_release-disk001.vmdk`.
    Right click on that entry and click `Remove Attachment`.
 4. Click `Ok` in the storage settings window.
 5. Right click on the "infosec_forensics_release" virtual machine and click `Remove...` in the context menu.
 6. The "VirtualBox - Question" popup interface will ask if you want to delete files associated with the virtual
    machine. Click on the `Delete all files` button.
 7. You can now repeat the steps from the previous two sections, using the saved copy of `infosec_forensics_release.ova`
    instead of downloading it again.
