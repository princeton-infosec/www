---
title: "Assignment Six: Digital Forensics (Group Assignment)"
summary:    "In this project, you will play the role of a forensic analyst who is investigating a fictitious murder.
            Your job is to conduct a forensic examination of a disk image and document any evidence related to the
            murder."

type: "page"

reading_time: false
share: false
profile: false
comments: false
---

In this project, you will play the role of a forensic analyst who is investigating a fictitious murder. Your job is to
conduct a forensic examination of a disk image and document any evidence related to the murder. 

The accused is Bob, who has fled the country and disappeared. Officers seized a disk image of Bob's computer, which is
all you have for this investigation.

### Getting Started

You will again use the [same VM as the previous assignments]({{< ref "student_vm" >}}) to complete this
assignment. This time, however, you have a second virtual machine to explore. This second virtual machine is the "disk
image" of Bob's computer. See [these installation instructions]({{< ref "forensics_vm" >}}) for a step-by-step
guide to setting up the forensics image and connecting it to your first virtual machine.

## Tasks and Deliverables

The entirety of this assignment is simply to collect information related to the murder from the provided disk image. As
you conduct the investigation, you will encounter "tokens" that represent major steps or important files. Most of the
tokens are a string of words at the end of a file, separated by a single character each. Other tokens are simply
passwords or monetary transaction identifiers. You should record these tokens in a plain text file called `tokens.txt`,
separating each token with a new line. For each token you find, you must also **write two or three concise sentences**
describing how you found the token and why it is significant. This file is the primary source of your grade. For full
credit, you must submit the following tokens:

 * At least 1 password.
 * At least 6 two-word tokens (out of 8 total)
 * At least 3 three-word tokens (out of 4 total)
 * At least 2 four-word tokens (out of 3 total) **OR** 1 four-word token and 1 monetary transaction identifier

Whenever you find a piece of evidence (e.g., a token), you must include a file or screenshot demonstrating where you
found it. This evidence will go into an `evidence` directory that you submit. This directory should contain things like
files containing tokens, screenshots of where you found a token, programs you wrote to crack passwords, etc. You should
not need more than one file in this directory per token you found.

## Policies and Hints

**Collaboration outside of your group** is strictly limited to conceptual topics discussed in lecture. Much of this
assignment is focused on a group exploring possibilities and trying things themselves. The evidence you find, techniques
you attempt, the success or failure of each attempt, etc. are all considered part of your solution. This means sharing
that information outside of your group will violate expectations of academic integrity. You are free to search online
for generic techniques and tutorials so long as you respect the collaboration policy of this course and cite all
sources.

**Cracking passwords** is a required task in this project. It is sufficient to bruteforce these passwords (i.e., guess
and check). It is highly recommended you use [this
dictionary](https://gist.githubusercontent.com/h3xx/1976236/raw/bbabb412261386673eff521dddbe1dc815373b1d/wiki-100k.txt)
for such password cracking.

**Network attacks** are *not* required in this project. You should not conduct any network attacks. Other than using
online informational resources, you do not even need an internet connection to complete this project.

## Submission Requirements

You will submit a single zip file to [Gradescope](https://www.gradescope.com) containing your `tokens.txt` file and your
`evidence` directory.

* `tokens.txt` - Plain text file containing all the tokens you discover and brief explanations for each.
* `evidence/...` - Directory containing recovered evidence files.
