---
title: "Assignment Four: Network Security (Group Assignment)"
summary:    "This project will introduce you to common network protocols, network packet trace analysis, and the basics
            of network penetration testing."

type: "page"

reading_time: false
share: false
profile: false
comments: false
---

This project will introduce you to common network protocols, network packet trace analysis, and the basics of network
penetration testing. This is a group assignment and must be done in groups of two or three only. You will complete three
parts in this assignment, and each group member must contribute to all of the parts.

| Assignment Sections                                                                  |    |
|--------------------------------------------------------------------------------------|----|
| [Part 1: Exploring Network Traces]({{< ref "#part-1-exploring-network-traces" >}})   | You will use a network analyzer to answer several questions about network traffic that has been captured and provided to you as a `pcap` file. |
| [Part 2: Network Anomaly Detection]({{< ref "#part-2-network-anomaly-detection" >}}) | You will write a program that analyzes data in `pcap` files to determine if any host has performed a specific type of network scan. |
| [Part 3: Penetration Testing]({{< ref "#part-3-penetration-testing" >}})             | You will play the role of a security consultant testing a corporate network for network vulnerabilities, answering questions about your findings. |

### Disclaimer

This project asks you to perform several tasks that would be considered network attacks in many situations. For this
project, you have our permission to target a network that we are providing for this purpose. Attempting the same kinds
of attacks against other networks without authorization is prohibited by law and university policies. You must not
attack any network without explicit permission from an authorized administrator of that network. Understand what actions
are considered "out of scope" for [Part 3]({{< ref "#pentesting-scope" >}}) of the assignment.

You must also understand that observing the work of other groups (even if visible on shared network infrastructure) will
be considered academic misconduct. See ["Rights, Rules, and Responsibilities"](https://rrr.princeton.edu/) on the
Princeton University website for official policies regarding privacy and property rights.

### Getting Started

Please follow the [Student VM Instructions]({{< ref "student_vm" >}}) to setup a virtual environment with pre-installed
tools needed for this project. Once the setup is complete and you are able to log into the virtual machine, download the
assignment files to the virtual machine and proceed to the following parts. You will answer questions in the text files
found in that starter zip archive.

## Part 1: Exploring Network Traces

Security analysts and attackers both frequently study network traffic to search for vulnerabilities and to characterize
network behavior. In this section, you will examine a network packet trace (commonly called a `pcap` or "packet
capture") that we recorded on a sample network for this assignment. You will search for specific behaviors and relevant
details using the [Wireshark](https://www.wireshark.org) network analyzer, which is installed on the provided VM image.

Examine the `part1.pcap` file from the assignment files. Familiarize yourself with Wireshark's features. Try exploring
the various options for filtering and reconstructing data streams. Don't be afraid to search for introductory tutorials
or guides about these features.

Concisely answer the questions below. Each response should require at most two or three sentences. Use the provided
`part1.txt` file for your answers.

1. Multiple devices are connected to the local network. What are their MAC and IP addresses? Who manufactured these devices?
2. What type of network does this appear to be (e.g., a large corporation, an ISP back- bone, etc.)? Point to evidence from the trace that supports this.
3. One of the clients connects to an FTP server during the trace.
    1. What is the DNS hostname of the server it connects to?
    2. Is the connection using Active or Passive FTP?
    3. Based on the packet capture, what is one major vulnerability of the FTP protocol?
    4. Name at least two network protocols that can be used in place of FTP to provide secure file transfer.
4. The trace shows that at least one of the clients makes HTTPS connections to sites other than Facebook. Pick one of these connections and answer the following:
    1. What is the domain name of the site the client is connecting to?
    2. Is there any way the HTTPS server can protect against the leak of information in (a)?
    3. During the TLS handshake, the client provides a list of supported cipher suites. List the first three cipher suites and name the crypto algorithms used in each.
    4. Are any of these cipher suites worrisome from a security or privacy perspective? Why?
    5. What cipher suite does the server choose for the connection?
5. One of the clients makes a number of requests to Facebook.
    1. Even though logins are processed over HTTPS, what is insecure about the way the browser is authenticated to Facebook?
    2. How would this let an attacker impersonate the user on Facebook?
    3. How can users protect themselves against this type of attack?
    4. What did the user do while on the Facebook site?

## Part 2: Network Anomaly Detection

In Part 1, you manually explored a network trace. Now, you will programmatically analyze a pcap file to detect
suspicious behavior. Specifically, you will be attempting to identify port scanning.

Port scanning is a technique used to find network hosts that have services listening on one or more ports. It can be
used offensively to locate vulnerable systems in preparation for an attack or defensively by researchers and
administrators. In one kind of port scanning technique, known as a `SYN` scan, the scanner tends TCP `SYN` packets and
watches for hosts that respond with a `SYN+ACK` packets ([see the NMAP documentation for more details on this type of
scan](https://nmap.org/book/synscan.html)). Since most hosts are not prepared to receive connections, most of the `SYN`
packets will be unanswered. This means you will likely observe a host who is scanning the network will send many more
`SYN` packets than the number of `SYN-ACK` packets it receives.

Your task is to develop a Python program that analyzes a pcap file in order to detect possible `SYN` scans. To do this,
you should use a library called [`dpkt`](https://github.com/kbandla/dpkt). This library is included in the provided VM
image. You can find helpful tutorials on its [documentation
website](https://dpkt.readthedocs.io/en/latest/examples.html).

Your program (called `detector.py`) must take as a command-line argument the path of a pcap file to be analyzed. A
skeleton file for `detector.py` is included in the assignment files. For example:

```bash
python detector.py capture.pcap
```

The output should be the set of IP addresses (one per line) that sent more than three times as many `SYN` packets as the
number of `SYN+ACK` packets they received. Your program should **silently ignore** packets that are malformed or using
**another protocol** besides TCP.

A large (~350 MB) sample pcap file is included in the assignment files. It is called `part1.pcap`. You can examine the
packets manually by opening this file in Wireshark. For this input, your program's output should be these lines (in any
order):

```
128.3.23.2
128.3.23.5
128.3.23.117
128.3.23.158
128.3.164.248
128.3.164.249
```

We will test your program using a variety of input pcap files to make sure your program works with arbitrary network
traces. Although the above instructions are for a Python program, you may use another programming language. If this is
the case, simply make a post on Piazza to inform the AIs of which programming language and which packet parsing library
you plan to use.

## Part 3: Penetration Testing

In this section, you are tasked with providing penetration testing services to a fictional company called SketchyCorp.
Each project team will conduct a thorough penetration test of the company's networks and exposed systems.

SketchyCorp recently created a remote office for its employees to work. SketchyCorp is concerned that its remote office
may be more vulnerable than its headquarters since it uses a wireless network to provide access to its remote employees.

Your objective is to test the security of SketchyCorp's networks and systems. In this engagement you will be authorized
to break into SketchyCorp's systems and explore any vulnerabilities you find. As in a real-world penetration test, you
will be expected to use your ingenuity and technical skills to discover clues and techniques for meeting your
objectives.

SketchyCorp employees connect to the wireless network using WPA2-PSK security. From there, they can access the
SketchyCorp firewall, which allows company employees to log into and gain access to company mainframe. The investigators
believe the infrastructure works as shown in this figure.

{{< figure src="netsec_infrastructure.png" library="img" >}}

We have sent an investigator to the remote office for wireless access analysis. However, they are foiled by the WPA2-PSK
security. They have managed to capture the wireless authentication handshake, which is found in `part3.pcap` in the
assignment files. They have also determined that the password is in the form of either cos432-XYZ or COS432-XYZ where X,
Y, and Z are alphanumeric characters. Provide them with the correct WPA password with the form at [this
website](https://www.cos432.org/netsec), and they will provide you with your next lead there.

Concisely answer the questions below. Each response should require at most two or three sentences. Use the provided
`part3.txt` file.

1. How many possible Wi-Fi passwords are there that fulfills the password format?
2. What is that actual Wi-Fi password used? How did you obtain that password?
3. There are three machines in the network, namely the employee, the firewall, and the mainframe.
    1. What are their IP addresses, respectively? (If a machine has multiple interfaces, report all IP addresses.) How did you get these addresses?
    2. What services are currently running on these machines respectively? On which port is each service running? How did you obtain this information?
4. There are unencrypted and encrypted conversations between Alice and Bob.
    1. What does the unencrypted conversation say? Please paste it here and briefly describe how you found and obtained the content.
    2. (extra credit) Can you decrypt the encrypted messages? If so, what does the plaintext say? Describe how you determined the plaintext contents of the encrypted messages.
5. List all of the clients of the company. Briefly explain how you gained access to this client list.

### Pentesting Scope

There will be certain systems and networks that are *in scope* for this project. Everything else should be considered
*out of scope*. If you have any questions about what is in or out of scope for this project, get clarification from one
of the course AIs before you act.

These are *in scope*:

* Connecting to the SketchyCorp network.
* Capturing network traffic from the SketchyCorp firewall.
* Using automated network scanning tools from the SketchyCorp firewall.
* Logging into SketchyCorp systems with any credentials you obtain.
* Brute forcing or cracking SketchyCorp passwords with your local machines.

Here are a few guidelines to avoid going *out of scope* during this project:

* DO NOT brute force any passwords if it means sending large amounts of traffic over a network connection.
* DO NOT use vulnerability scanners or packaged exploits (e.g., you cannot use Metasploit)
* DO NOT capture traffic on you local machine with Wireshark or `tcpdump`. Additionally, `nmap` may not be run on your
  local machine at all. You may only use Wireshark on your local machine to review packet capture files.
* DO NOT perform actions that cause difficulty for other users or that interfere with the project infrastructure (i.e.,
  executing a denial of service attacks).
* DO NOT attempt to elevate your shell privileges on the SketchyCorp firewall.
* DO NOT do anything else that is not specifically designated as *in scope*. If you are unsure, please ask for
  clarification on Piazza.

**A note about cheating:** There may be backdoors you discover along the way. DO NOT SHARE THEM. If you have questions
about whether you may use a particular backdoor, post a private question on Piazza *before* using it.

## Submission Requirements

Submit the following files to [Gradescope](https://www.gradescope.com):

* `part1.txt` - Answers to questions regarding Part 1.
* `detector.py` - Source code for your Python script (or another program) for `SYN` scan detection.
* `part3.txt` - Answers to questions regarding Part 3.
