---
title: Assignments
summary: "List of course assignments."

reading_time: false
share: false
profile: false
comments: false
---

| Assignment                                                         | Due Date   | Type of Assignment                | Assignment Files                                                              |
|--------------------------------------------------------------------|------------|-----------------------------------|-------------------------------------------------------------------------------|
| [1. Pseudo-Random Generator]({{< ref "a1_prg" >}})                 |            | Individual Programming Assignment | {{< staticLink "student_files/a1.zip" "a1.zip" >}} a1.zip {{< /staticLink >}} |
| [2. Asymmetric Cryptography]({{< ref "a2_asymmetric" >}})          |            | Individual Programming Assignment | {{< staticLink "student_files/a2.zip" "a2.zip" >}} a2.zip {{< /staticLink >}} |
| [3. Authenticated Channel]({{< ref "a3_authenticated_channel" >}}) |            | Individual Programming Assignment | {{< staticLink "student_files/a3.zip" "a3.zip" >}} a3.zip {{< /staticLink >}} |
| [4. Network Security]({{< ref "a4_network_security" >}})           |            | Group Activity Assignment         | {{< staticLink "student_files/a4.zip" "a4.zip" >}} a4.zip {{< /staticLink >}} |
| [5. Web Security]({{< ref "a5_web_security" >}})                   |            | Group Activity Assignment         | {{< staticLink "student_files/a5.zip" "a5.zip" >}} a5.zip {{< /staticLink >}} |
| [6. Digital Forensics]({{< ref "a6_forensics" >}})                 |            | Group Activity Assignment         | {{< staticLink "student_files/a6.zip" "a6.zip" >}} a6.zip {{< /staticLink >}} |
